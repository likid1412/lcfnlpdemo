//
//  ViewController.m
//  LCNLPDemo
//
//  Created by Likid on 28/12/2016.
//  Copyright © 2016 Likid. All rights reserved.
//

#import "ViewController.h"
#import "LCNLPTimeProcessor.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *sentence = @"08年北京申办奥运会，8月8号开幕式，九月十八号闭幕式。"
    "一年后的七月21号发生了件大事。"
    "今天我本想去世博会，但是人太多了，直到晚上9点一刻人还是那么多。"
    "考虑到7/4/16 19:01和后天14:05人还是那么多，决定下周日晚上10:15:21再去。"
    "恢复自我美德的时代，充满美丽、希望、挑战的上，我们所处的敏感时期会有一个人在午时出现拯救世界。"
    "国庆节晚上八点"
    "我有一点饿";
    
    // 后天14:05
    sentence = @"后天14:05";
    
//    NSLog(@"sentence: %@\n", sentence);
    
//    LCNLPTimeProcessor *timeProcessor = [[LCNLPTimeProcessor alloc] init];
//    [timeProcessor loadTimeModel];
//    [timeProcessor parseTimePharseForSentence:sentence];
    
//    LCNLPTimeCheckingResult *firstFutureResult = [LCNLPTimeProcessor firstFutureResultWithSentence:sentence];
    
    LCNLPTimeCheckingResult *earliestFutureResult = [LCNLPTimeProcessor earilestFutureResultWithSentence:sentence];
    NSLog(@"earliestFutureResult: %@", earliestFutureResult);
    
}

@end
